# nextpnr-xilinx

Installation instructions for Nextpnr-xilinx with Digilent Arty A7-100t
with Xilinx Artix7 100T

Use of the automated install scripts recommended

* Source code: <https://github.com/gatecat/nextpnr-xilinx>
* Bugzilla page <https://bugs.libre-soc.org/show_bug.cgi?id=790>
* Auto-install script: <https://git.libre-soc.org/?p=dev-env-setup.git;a=blob;f=nextpnr-xilinx-install;hb=HEAD>
* Prerequisites: yosys <https://git.libre-soc.org/?p=dev-env-setup.git;a=blob;f=hdl-tools-yosys;hb=HEAD>

# Setting up new debootstrap and chroot into it

Run the following if you wish to isolate the nextpnr-xilinx build
from other software (reproducible builds) or use the schroot
auto-preparation script here:
<https://git.libre-soc.org/?p=dev-env-setup.git;a=blob;f=mk-deb-chroot;hb=HEAD>

    export MY_CHROOT=/stable-chroot
    mkdir $MY_CHROOT
    debootstrap buster $MY_CHROOT http://deb.debian.org/debian/
    mount -t proc proc $MY_CHROOT/proc
    mount -t sysfs sysfs $MY_CHROOT/sys
    mount -t devpts devpts $MY_CHROOT/dev/pts/
    chroot $MY_CHROOT /bin/bash

# Download neccessary softwares

    apt-get install -y libcurl3-gnutls/buster git/buster

    git clone https://github.com/f4pga/prjxray.git
    git clone https://github.com/SymbiFlow/prjxray-db.git
    git clone https://github.com/gatecat/nextpnr-xilinx.git

# Steps to compile prjxray

## Necessary software to install

    apt-get install -y build-essential make cmake python3 python3-dev \
     python3-setuptools python3-numpy cython3 python3-pip

    pip3 install textx fasm

## Build prjxray

    cd prjxray
    git checkout 18b92012afe2b03f3f975a78c4372c74b60dca0c
    git submodule update --init --recursive
    mkdir build; cd build
    cmake -DCMAKE_INSTALL_PREFIX=/usr/local/nextpnr-xilinx ..
    make -j$(nproc)
    make install
    install -d -m 0755 /usr/local/nextpnr-xilinx/build/tools
    install -m 0755 tools/{bitread,bittool,frame_address_decoder,gen_part_base_yaml,segmatch,xc7frames2bit,xc7patch} \
     /usr/local/nextpnr-xilinx/build/tools
    cd ..
    cp -dpr utils /usr/local/nextpnr-xilinx
    sed -i -e '/^# Vivado /,$d' \
     /usr/local/nextpnr-xilinx/utils/environment.sh
    python3 setup.py develop
    export PATH=/usr/local/nextpnr-xilinx/bin:$PATH
    cd ..

# Steps to compile prjxray-db

## Install prjxray-db

    cd prjxray-db
    git archive --format=tar --prefix=database/ \
      0a0addedd73e7e4139d52a6d8db4258763e0f1f3 | \
      tar -C /usr/local/nextpnr-xilinx -xf -
    cd ..

# Steps to compile nextpnr-xilinx

## Necessary software to install

    apt-get install -y libboost-thread-dev libboost-iostreams-dev \
     libboost-program-options-dev libeigen3-dev libboost-python-dev \
     libboost-filesystem-dev

## Build nextpnr-xilinx

    cd nextpnr-xilinx
    git checkout 565588a69ea95a52f7c7592f4ed81d9bef6cfb60
    cmake -DARCH=xilinx -DBUILD_GUI=OFF -DCMAKE_INSTALL_PREFIX=/usr/local/nextpnr-xilinx .
    make -j$(nproc)
    make install
    python3 xilinx/python/bbaexport.py --device xc7a100tcsg324-1 \
     --bba xilinx/xc7a100t.bba
    ./bbasm --l xilinx/xc7a100t.bba xilinx/xc7a100t.bin
    install -d -m 0755 /usr/local/nextpnr-xilinx/share/xilinx
    install -m 0755 xilinx/xc7a100t.bin \
     /usr/local/nextpnr-xilinx/share/xilinx
    export XRAY_DIR=/usr/local/nextpnr-xilinx

    # build attosoc example; it should build attosoc.bit as final bitstream

    cd xilinx/examples
    cp -dpr arty-a35 arty-a100
    cd arty-a100
    sed -i -e 's@xc7a35tcsg324-1@xc7a100tcsg324-1@g' \
     -e 's@../../../nextpnr-xilinx@nextpnr-xilinx@g' \
     -e 's@../../xc7a35t.bin@/usr/local/nextpnr-xilinx/share/xilinx/xc7a100t.bin@g' \
     attosoc.sh
    ./attosoc.sh

    # see file attosoc.bit; it is the bitstream file and if built then success
