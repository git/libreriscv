# Call for Participation

- Web version of this CfP:
  <https://libre-soc.org/conferences/fosdem2024/fosdem2024_cfp/>

This email is a Call For Participation in the
**Libre-SOC, FPGA and VLSI Devroom** at FOSDEM 2024.

Key dates:

- Friday, 1st December, 00:00 (CET, UTC+1) - CfP deadline
- Friday, 15th December, 00:00 (CET, UTC+1) - Devroom schedule and
final speaker confirmation
- **Saturday, 3rd February** - First day of FOSDEM 2024
- Saturday, 3rd February - Libre-SOC, FPGA and VLSI Devroom date
- **Sunday, 4th February** - Second day of FOSDEM 2024

## Useful links

- Libre-SOC, FPGA and VLSI Devroom Schedule:
  <https://fosdem.org/2024/schedule/track/libre-soc-fpga-and-vlsi/>
- Libre-SOC homepage:
  <https://libre-soc.org>

## CFP Introduction

This devroom covers topics mostly related to hardware design
(FPGA and VLSI/ASIC).

Libre-SOC is a major project in the open-hardware/free-software
space, and will be sharing progress made so far in areas of efficient
SVP64 assembler, formal verification, and general Power ISA
development (proposed advanced biginteger instructions, etc.)
 
Libre/Open VLSI, very excitingly, has begun to take off, recently.
NLnet sponsors several VLSI-related projects (Cell Libraries, HDL, VLSI
tools), Efabless and Skywater bring 90nm, 130nm, 180nm MPWs
to the masses.

Please distribute widely to interested parties: anything related to
VLSI and Libre/Open Tape-outs and Silicon development is welcome.

We are looking for any libre or open source topic relating to
hardware design, including but not limited to:

- Chip Design (including nanoscale 3D printing)
- VLSI and FPGA Hardware Design
- Libre/Open Processors in FPGA and/or ASIC
- ISA design and usage

This year we welcome talks that are either:

* 10 min (lightning talk, short introduction of project/topic)
* 25 min + 5 min Q&A (normal slot)
* 40 min + 10 min Q&A (extraordinary slot for a topic that needs a
deeper explanation)

### What is FOSDEM?

**FOSDEM is a free event for software developers to meet, share ideas
and collaborate.**

Every year, thousands of developers of free and open source software
from all over the world gather at the event in Brussels.

FOSDEM 2024 will take place on Saturday 3 and Sunday 4 February 2024.
It will be an in-person event at the **ULB Solbosch Campus, Brussels,
Belgium, Europe.**
If you are unable to attend in-person you can watch any live stream
from the main tracks and developer rooms.

### Important stuff:

- FOSDEM is free to attend. There is no registration.
- [FOSDEM website](https://fosdem.org/)
- [FOSDEM code of conduct](https://fosdem.org/2024/practical/conduct/)
- [FOSDEM Schedule](https://fosdem.org/2024/schedule/)

## Desirable topics:

This devroom welcomes anything related to making an ASIC,
or designing one, or using FPGAs, or developing
an FPGA Board, or developing tools and techniques that make VLSI ASIC
design easier: we'd love to hear from you.  Here's a list of topics:

- Open Hardware projects
- ISA and Architecture design
  * Simplified Vector extensions
- VLSI ASIC Design and Manufacture
  * Libre/Open DIY Foundries (nanoscale 3D printing)
  * Libre/Open VLSI tools and toolflow
  * Libre/Open VLSI Cell Libraries
  * VLSI Simulation and Verification
- VLSI Tools in use or in development
  * Silicon-proven (QFlow, coriolis2, OpenLANE)
  * Under development (LibreEDA, other)
  * Advances in Algorithmics in Place and Route and Layout
- FPGAs
  * Libre/Open FPGA designs
  * FPGA toolchains and Reverse-Engineering
  * FPGA workflow
- VLSI RTL and HDL
  * Advanced and innovative alternative HDL tools
  * Formal Correctness Proofs
  * Testing methodologies
  * Hardware Trust (and how to break it)
- Software Engineering as applied to Hardware
  * Continuous Integration for VLSI
  * Automated tool development (RTL to GDS-II)
  * Automated testing

### Topic overlap

There is quite a lot of overlap this year with:

* [Open Hardware and CAD/CAM](https://fosdem.org/2024/schedule/track/open-hardware-and-cadcam/),
* [Embedded, Mobile and Automotive](https://fosdem.org/2024/schedule/track/embedded-mobile-and-automotive/),
* [Open Source Firmware, BMC and Bootloader](https://fosdem.org/2024/schedule/track/open-source-firmware-bmc-and-bootloader/),

## How to submit your proposal

To submit a talk, please visit the [FOSDEM 2024 Pretalx
website](https://pretalx.fosdem.org/fosdem-2024/cfp).

Click **Submit a proposal**.

Make sure to choose the **Libre-SOC, FPGA and VLSI**
devroom in the track drop-down menu (otherwise we might miss it).

### What should be in your submission

- Name
- Short bio
- Contact info
- Title (funny titles are ~~required~~/appreciated)
- Abstract (what you're going talk about, supports markdown)
- Duration

On the second page of the submission form:

- License, please use **CC BY-SA 4.0**.
- Resources to be used during the talk

### Optional:

- Description (more detail description, supports markdown)
- Submission notes (for the organiser only, not made public)
- Session image (if you have an illustration to go with the talk)
- Additional speaker/s (if more than one person presenting)

The second page will include fields for:

- Extra materials for reviewers (optional, for organisers only)
- Additional information about the speaker (optional).

### Recordings

*(thank you to the Open Hardware and CAD/CAM devroom for this snippet)*

The talks will be recorded and live-streamed during FOSDEM2024.
The recordings will be published under the same licence as all
FOSDEM content (CC-BY). Only presentations will be recorded,
not informal discussions and whatever happens during breaks
between presentations.

We require affirmative consent to this license.  Please include the
following statement in your submission notes:

```
If my presentation is accepted for FOSDEM, I hereby agree to license
all recordings, slides, and other associated materials under the
Creative Commons Attribution Share-Alike 4.0 International License.
Sincerely,
<NAME>
```

Once you've completed the required sections, submit and we'll get
back to you soon!

## Things to be aware of

* The reference time will be Brussels local lime (CET)
  <https://www.timeanddate.com/worldclock/belgium/brussels>
* There will be a Q/A session after the talk is over.
  Please make sure that you will be available on the
  day of the event.
* If you're not able to attend the talk in-person, live stream links
  will be available on the FOSDEM schedule page:
  <https://fosdem.org/2024/schedule/>.
* FOSDEM Matrix channels are specific to each devroom,
  the general link is: <https://matrix.to/#/#fosdem:fosdem.org>
* **Matrix bridge to the LibreSOC IRC channel**:
  <https://matrix.to/#/#_oftc_#libre-soc:matrix.org>
* FOSDEM 2024 FAQ: <https://fosdem.org/2024/faq/>

## Important Dates

- **December 1st: submission deadline**
  - FAQ - is the submission deadline final?
    - Yes. The new submission system is stricter, and thus the,
      1st of December is the final date.
- ASAP: announcement selected talks (15th Dec or earlier)
- February 3rd: FOSDEM! (with live Q&A during recorded talks)

## Contact us

- [Andrey Miroshnikov](mailto:andrey at technepisteme.xyz)
  - "octavius" on #fosdem and #libre-soc Libera.Chat IRC
- [Sadoon Albader](mailto:sadoon <sadoon@albader.co>)
  - "sadoon[m]1" on #libre-soc Libera.Chat IRC
- [Luke Leighton](mailto:lkcl at lkcl.net)
  - "lkcl" or "mwfc" on #fosdem Libera.Chat IRC


