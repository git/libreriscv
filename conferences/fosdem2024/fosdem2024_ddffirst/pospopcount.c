/*Copyright (c) 2020 Robert Clausecker fuz@fuz.su
  count8 reference implementation */
count8safe(counts *[8]int, buf []uint8) 
  for i := range buf
    for j := 0; j < 8; j++
      counts[j] += int(buf[i] >> j & 1)
