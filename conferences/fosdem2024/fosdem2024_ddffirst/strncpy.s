    mtspr 9, 3  # move r3 to CTR
    addi 0,0,0  # initialise r0 to zero
L1: # chr-copy loop starts here:
    setvl 1,0,64,0,1,1 # VL,r1 = MIN(CTR,MVL=64)
    # load VL bytes (update r10 addr)
    sv.lbzu/pi *16, 1(10)    
    sv.cmpi/ff=eq/vli *0,1,*16,0 # cmp 0, chop VL
    # store VL bytes (update r12 addr)
    sv.stbu/pi *16, 1(12)
    sv.bc/all 0, *2, L1  # stop if cmpi failed
L2: # zeroing loop starts here:
    setvl 1,0,64,0,1,1 # VL,r1 = MIN(CTR,MVL=64)
    # store VL zeros (update r12 addr)
    sv.stbu/pi 0, 1(12)
    sv.bc 16, *0, L2 # dec CTR by VL
