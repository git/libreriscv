# Pypowersim/ISACaller tutorial - In progress!

Useful Links (Libre-SOC):

* (**Very Useful**) OpenPOWER ISA Pseudo-code:
<https://libre-soc.org/openpower/isa/>

Useful Links (External):

* PowerPC Arch assembler lecture:
<https://www.eecs.umich.edu/courses/eecs373.w04/Lectures/stever_old_lectures/lec2.pdf>
* Hello world in PPC64 assembly:
<https://github.com/matja/asm-examples/blob/master/ppc64/hello.ppc64.linux.syscall.gas.asm>
* Hello world in PPC64LE assembly:
<https://gist.github.com/sandip4n/09b50786e88968faaecdf42360c85b1b>

This tutorial is intended to get started with Libre-SOC's in-house instruction
simulator. The main Python class doing the work is called `ISACaller`, while
the more comprehensive wrapper file used to run binaries is called
`pypowersim`.

## `-->` START HERE `<--` Run `ISACaller` unit tests first!

TODO: Document tutorial.

Since `pypowersim` is much more involved (as it requires PC, register setup,
etc.), it is *strogly* encouraged to first write a basic unit test involving
the `ISACaller` class.

### Find appropriate umbrella `test_caller_[FUNCTION].py` class

The directory `openpower-isa/src/openpower/decoder/isa/` contains a set of files
for running unit tests using pytest. If adding tests for a new category, a new
umbrella will need to be created.

The umbrella file has name of the form `test_caller_[FUNCTION].py`.
For example of the ALU test caller, see
[`test_caller_alu.py`](https://git.libre-soc.org/?p=openpower-isa.git;a=blob;f=src/openpower/decoder/isa/test_caller_alu.py;hb=HEAD).

It's suggested to copy the contents of an existing test caller file.

### Write unit tests under the `src/openpower/test/[FUNCTION]/` directory

Once a test caller class exists, the actual tests reside under the
`src/openpower/test/[FUNCTION]/` directory. For example of the ALU tests, see
[alu_cases.py](https://git.libre-soc.org/?p=openpower-isa.git;a=blob;f=src/openpower/test/alu/alu_cases.py;hb=HEAD)

Copy an existing file when writing new tests, which should gradually teach you
how to use the `ISACaller` class.

### Example of new test - system call instructions



## Setup a Debian 10 chroot environment

**Skip this section if `pypowersim` is already present on your system.**

Setup new chroot:

    $ cd dev-env-setup
    $ sudo bash
    # ./mk-deb-chroot isacaller
    # ./mk-deb-chroot isacaller
    # exit
    $ schroot -c isacaller
    (isacaller):$ cd dev-env-setup
    (isacaller):$ sudo bash
    (isacaller):# ./install-hdl-apt-reqs
    (isacaller):# ./hdl-tools-yosys
    (isacaller):# ./hdl-dev-repos
    (isacaller):# ./binutils-gdb-install
    (isacaller):# exit

*(NOTE to self: check if `hdl-dev-repos` actually necessary)*

From here on, `pypowersim` should be in your `$PATH` and can simply be called
from your terminal (when inside the newly created chroot).

    (isacaller):$ pypowersim --help

Help message (may change, so try yourself):

    -i --binary=   raw (non-ELF) bare metal executable, loaded at 0x0
    -a --listing=  file containing bare-metal assembler (no macros)
    -g --intregs=  colon-separated file with GPR values
    -f --fpregs=   colon-separated file with FPR values
    -s --spregs=   colon-separated file with SPR values
    -l --load=     filename:address to load binary into memory
    -d --dump=     filename:address:len to binary save from memory
    -q --qemu=     run qemu co-simulation
    -p --pc=       set initial program counter
    -h --help      prints this message
    notes:
    load and dump may be given multiple times
    load and dump must be 8-byte aligned sizes
    loading SPRs accepts SPR names (e.g. LR, CTR, SRR0)
    numbers may be integer, binary (0bNNN) or hex (0xMMM) but not FP
    running ELF binaries: load SPRs, LR set to 0xffffffffffffffff
    TODO: dump registers
    TODO: load/dump PC, MSR, CR
    TODO: print exec and sub-exec counters at end

## Running existing example

To start with, let's see how a comprehensive example works. A good demonstrator
of the capabilities of SVP64 is the XChaCha20 encryption algorithm. (Difference
between ChaCha20 and XChaCha20 being an
[extended 192-bit nonce](https://crypto.stackexchange.com/a/101505)).

This page will go into the details of running the simulator, not the SVP64
specifics. Please see the SVP64 Cookbook page on
[ChaCha20](https://libre-soc.org/openpower/sv/cookbook/chacha20/)
for more detailed information on the algorithm and SVP64 features.

To run the example.

    (isacaller):$ cd ~/src/openpower-isa/crypto/chacha20
    (isacaller):$ make
    (isacaller):$ ./test-chacha20

Or with `SILENCELOG=1` if you want less terminal output from the simulator:

    (isacaller):$ SILENCELOG=1 ./test-chacha20

## Explanation of the process

Konstantinos
[summarising the process](https://libre-soc.org/irclog/%23libre-soc.2023-09-10.log.html#t2023-09-10T18:44:49).


