# Tuesday 9nd April 16:00 UTC

* Previous notes: [[meetings/sync_up/sync_up_2024-04-02]]
* Next week's notes: [[meetings/sync_up/sync_up_2024-04-16]]

# Main Agenda

Meeting notes:

# Cesar

Got Jacob's UART demo working on the Nexys Video, then got ls2 Hello World and 
Coldboot to work, as well (UART only).
See: [bug #1004](https://bugs.libre-soc.org/show_bug.cgi?id=1004#c19)

Sent a [pull request](https://gitlab.com/nmigen/nmigen-boards/-/merge_requests/6) 
to merge the current board definition file into nmigen-boards.
Will fill out the remaining of the board definition file based on Litex.

Will use try..catch to import it into ls2 while Nexys Video support
is not yet upstream in nmigen-boards.

# Jacob

absent 

# Luke

attended

# Dmitry

Absent. The only minor update I have so far is that Michiel contacted me and confirmed they had received the proposal.

# Rita

attended
