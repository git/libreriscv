# Wednesday 22nd November 09:00 UTC

* Previous weeks' notes: [[meetings/sync_up/sync_up_2023-11-14]]
* Yesterday's notes: [[meetings/sync_up/sync_up_2023-11-21]]
* Next week's notes: [[meetings/sync_up/sync_up_2023-11-28]]

## Cesar

- Been visiting family, so not much progress.

- Plan to:
  - update FOSDEM travel costs
  - submit talk proposal
  - continue formal verif.

- When considering RISC-V ISA:
  - Binutils support - covered by new potential grant application
  - Compiler support - gcc, although initially assembly is primary

- FOSDEM:
  - Didn't get stand, could still bring FPGA and possibly do a live demo.
  - A new demo where hex file format could be uploaded to FPGA via UART?
    - Might be easier to just program the SPI flash separately
    - hex file commonly format (monitor program):
column, address, num of bytes, data, checksum

[[!tag meeting2023]]
[[!tag meeting_sync_up]]
