# Tuesday 19th March 17:00 UTC

* Previous notes: [[meetings/sync_up/sync_up_2024-03-12]]
* Next week's notes: [[meetings/sync_up/sync_up_2024-03-26]]

# Main Agenda

Meeting notes:

# Cesar

Focusing on [bug #1004](https://bugs.libre-soc.org/show_bug.cgi?id=1004), will
do nmigen-boards pinout.

Loopback test did work, moving on to more complex things.

Really good to have the board by my side, really speeds up development.

No worries about funding for it, the funds for [bug #1004](https://bugs.libre-soc.org/show_bug.cgi?id=1004) plus the Decred Jacob is sending me should be enough.

Will schedule an interview with Rita to help with her organizational research.

# Jacob

WIP paying Cesar with our remaining Decred for the FPGAs he recently bought. We had about 16 DCR, which only covers part of the cost.
(edit: paid.)

# Luke

didn't attend. was in A&E southampton.  unable to concentrate for 2 weeks on any work.

# Dmitry

didn't attend.

# Rita

Research data management, interviews and other PhD businesses.

[[!tag meeting2024]]
[[!tag meeting_sync_up]]

