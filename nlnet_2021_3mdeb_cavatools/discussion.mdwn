# 19th September 2023 - Diagram for proposed grant budget reallocation

*(Chart updated on 14nd Oct)*

The budgets outlined below have not all been put in the bug tracker, so that
NLnet can confirm whether proposed changes are appropriate first.

    Bug 939 - NLnet 2021 cavatools proposal 2021-08-071 - 50000
    |
    |-| (DONE) Bug 947 - switch reference assembler to machine-readable specs - 5500
    |                   RFP submitted, paid.
    |
    |-| (DONE) Bug 958 - provide support for missing instructions in binutils - 3000
    |                   RFP submitted, paid.
    |
    |- (DONE) Bug 976 - support missing specifiers - 1500
    |                   RFP submitted, paid.
    |
    |- (DONE) Bug 979 - Implement C-based Power ISA decoder compiler - 5500
    |                   RFP submitted.
    |
    |- (CONFIRMED) Bug 980 - Implement C-based Power ISA pseudocode compiler - 5500
    |                        Jacob can work on this one.
    |
    |- (IN PROGRESS) Bug 981 - Support PowerPC ABI in cavatools - 4500
    |                        Dmitry completed. Waiting on sign off.
    |
    |- (IN PROGESS) Bug 982 - Support PowerPC ABI in ISACaller - 4500
    |               (propose breakdown red 500, jacob 2500, dmitriy 1000, luke 500)
    |
    |-| (IN PROGRESS) Bug 983 - Support PowerPC SFFS compliance - 6000
    | |- (CONFIRMED) Bug 1169 - Add ELF and mmap support to ISACaller - 6000
    |                           **Is budget for 1169 coming from 983?**
    |
    |-| (PARTLY USED) Bug 984 - Support SVP64 in cavatools - 8000:
    | |- (DONE) Bug 1154 - Support basic PowerPC generated assembly - 2500
    | |                    RFP submitted.
    | |- Unallocated 5500 *Deduct this budget from grant.*
    |
    |- (CANCELLED) Bug 985 - Integrate cavatools into test API - 3000
    |                        *Deduct this budget from grant.*
    |
    |-| (IN PROGESS) Bug 987 - cavatools-related administrative activities - 3000
    | |- (DONE) Bug 997 - sorting out task schedule and MoU - 1000
    | |- (CONFIRMED) Bug 1126 - How to document new bugs/issues - 1000
    | |- (CONFIRMED) Bug 1171 - Improve budget-sync to generate HTML version of task_db/report - 1000
    |        Not started, might be worth to allocate to general admin instead.

Amount for NLnet to deduct from Cavatools grant:
`5500 (#984) + 3000 (985) = **8500EUR**`

## TODO

* Andrey: Look over 982, and see what needs to be done. Wait for confirmation
on task 1171, but should look at budget-sync program to see how it generates
the individual and grant reports.

* Dmitry: Please have a think about bugs 981, the new sub-task you proposed to
be under 984 (assume full budget of 5500). Don't modify 981, but feel free to
make additional comments.

* Jacob plan out what's required for 1169, and list the necessary tasks (stick
to bare minimum needed to get mmap support and a simple static elf to run).

