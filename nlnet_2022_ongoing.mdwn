# NL.net proposal

**This project is part of [NLnet NGI0 Entrust](https://nlnet.nl/entrust)
and has received funding from the European Union’s Horizon Europe research and innovation programme under grant agreement No 101069594.**

* 2022-08E-107
* [[nlnet_2022_ongoing/discussion]]
* <https://bugs.libre-soc.org/show_bug.cgi?id=961>

## Project name

Libre-SOC Ongoing 2022/3

## Website / wiki 

<https://libre-soc.org/nlnet_2022_ongoing>

# Summary

The funding to date from NLnet via EU Grants has been amazing and resulted
in significant development of Digitally-Sovereign VLSI designs.
Continuing to further that initial research to create High Performance
Compute for ultimate use in end-user products such as smartphones desktops
laptops and Industrial Embedded PCs is clearly important.
We therefore aim to further the IEEE754 Pipelines, associated Formal
Correctness Proofs, and continue implementing unit tests, Simulator,
Processor Core implementing Power ISA and Draft SVP64, as well as
documentation and attending conferences.

# Submitted to NLnet

Please be short and to the point in your answers; focus primarily on
the what and how, not so much on the why. Add longer descriptions as
attachments (see below). If English isn't your first language, don't
worry - our reviewers don't care about spelling errors, only about
great ideas. We apologise for the inconvenience of having to submit in
English. On the up side, you can be as technical as you need to be (but
you don't have to). Do stay concrete. Use plain text in your reply only,
if you need any HTML to make your point please include this as attachment.

## Abstract: Can you explain the whole project and its expected outcome(s).

Libre-SOC aims to create a Supercomputing-class entirely Libre Hybrid
CPU-VPU-GPU. In proposal 2022-08-51 we aim to begin the long process
of submitting the required Scalable Vector Extension to the OpenPOWER
Foundation: this Grant Request focusses more on continuing to
*implement* that Scalable Vector Extension.

With the entire project being 100% FOSSHW and managed strictly as a
Libre Project under strict Full Transparency conditions the end result
is a High-performance Processor Initiative that EU Citizens can trust.

# Have you been involved with projects or organisations relevant to this project before? And if so, can you tell us a bit about your contributions?

As mentioned in 2022-08-51,
a lot! a full list is maintained here <https://libre-soc.org/nlnet_proposals/>
and includes

* the world's first FOSSHW IEEE754 Formal Correctness Proofs for fadd, fsub, and fma, with support for FP Formal Proofs added to symbiyosis;
* the world's first in-place Discrete Cosine Transform algorithm;
* Significant improvements to Europe's only silicon-proven FOSSHW VLSI toolchain (coriolis2, by LIP6 Labs of Sorbonne University)
 to do an 800,000 transistor fully automated RTL2GDSII
tape-out;
* development of a 180nm Power ISA 3.0 "Test ASIC", the largest fully FOSSHW
  ASIC ever taped-out in Europe (and funded by Horizon 2020)
* development of an Interoperability "Test API" for Power ISA systems,
  with thousands of unit tests.

and much more. The side-benefits alone for EU citizens are enormous.

# Requested Amount    

EUR 100,000.

# Explain what the requested budget will be used for? 

(Note having completed 2022-02-012 we meet the conditions for a
larger budget request)

Whilst 2022-08-51 focusses on submitting SVP64 to the OpenPOWER ISA WG,
and satisfying Voting Members of its suitability, we need to proceed
with implementing SVP64 and underlying infrastructure:

* Dynamic Partitioned SIMD for nmigen
* Completion of IEEE754 FP Formal Correctness Proofs
* Completion of an In-Order Single-Issue core implementing SVP64
* Addition of the IEEE754 FPU to the Core
* Addition of other ALUs and pipelines (bitmanip, video)
  implementing new Draft instructions from 2022-08-051
* Addition of SMP (multi-core) support
* Running under Verilator and on FPGAs (big ones) which will
  need to be investigated, bought, and the Libre-Licensed tools support
  potentially added or improved
* Continued documentation, attendance of Conferences online
* Begin investigating Multi-Issue Out-of-Order, continuing
  the 6600 Scoreboard research from 2019-02-012
* Establishment and management of Continuous Integration
  infrastructure and upgrading the Libre-SOC IT systems
  (currently a single 4GB VM)
* If there is sufficient budget we would like to begin investigating
  OpenCAPI (we have access to two Bitmain 250 FPGAs thanks to UOregon)

several more practical details which help very much to ensure that the
efforts to date, funded very kindly by NLnet, reach fruition as part
of providing EU Citizens with a powerful Libre alternative processor
option.

# Compare your own project with existing or historical efforts.

As hinted at in 2022-08-051
we are basically developing a Cray-style Supercomputer, leveraging
the Supercomputing-class Power ISA
and extending it.  Similar historic ISAs include
Cray Y/MP, ETA-10, Cyber CDC 205. More recent is the NEC SX Aurora.
They are all proprietary systems: Libre-SOC's efforts are entirely
FOSSHW.

Whilst the European Processor Initiative is focussing exclusively
on RISC-V, due to the amount of time it takes to assess an ISA's
suitability it has to be said that it is being discovered, very slowly,
that RISC-V is not suited to High-Performance Supercomputing
workloads. The best explanation online is here:
<https://news.ycombinator.com/item?id=24459041>

Therefore this project is a really important alternative
being based on a much more suitable High-performance
base that has the backing of
IBM for over 25 years, and is now an Open ISA.
<https://openpowerfoundation.org/blog/final-draft-of-the-power-isa-eula-released/>

## What are significant technical challenges you expect to solve during the project, if any?

Processor design is HARD. This is dramatically underestimated.  We are
therefore taking a careful and considered incremental approach, using
Software Engineering programming techniques, developing unit tests
at every level and ensuring rigorous documentation and Project coordination
guidelines are adhered to.

We also make significant use of automation,
compiler technology and abstraction
which would never be considered by Hardware-only VLSI Engineers.
By taking a step back we simplify the approach to one that is
manageable by a much smaller team.

## Describe the ecosystem of the project, and how you will engage with relevant actors and promote the outcomes?

As in 2022-08-051
we are already set to submit presentations through multiple Conferences
as has been ongoing since 2019 as can be seen at <https://libre-soc.org/conferences> and will continue to submit press releases to
OPF <https://openpowerfoundation.org/blog/libre-soc-180nm-power-isa-asic-submitted-to-imec-for-fabrication/>. Our entire development is public
so is accessible to all.

# Extra info to be submitted

