#!/usr/bin/env python3

import os
import re
import sys
import subprocess
from pandocfilters import (toJSONFilter, RawInline, Space, Str, walk, Image,
                           Link)

"""
Pandoc filter for Markdown that converts most endnotes into
Pandoc's inline notes. If input notes had multiple paragraphs,
the paragraphs are joined by a space. But if an input note
had any blocks other than paragraphs, the note is left as is.
"""

# inkscape -z sv/bridge_phy.svg --export-png=bridge.png
out = open("/tmp/log.txt", "w")


def query(k, v, f, meta):
    global inlines
    if k == 'BlockQuote':
        inlines.append(v)
    elif isinstance(v, list):
        if inlines and k == 'Para':
            inlines.append(Space())
        inlines.extend(v)
    return v


def inlinenotes(k, v, f, meta):
    out.write("k v f meta %s %s %s %s\n" %
              (repr(k), repr(v), repr(f), repr(meta)))
    global inlines
    inlines = []
    if k == u'Image' and f == 'latex':
        imgname = v[2][0]
        out.write("     image %s\n" % (imgname))
        if imgname.startswith("/"):
            # use absolute paths so pandoc_img.py can be used in any directory
            file_path = os.path.abspath(__file__)
            openpower_path = os.path.split(file_path)[0]
            wiki_path = os.path.split(openpower_path)[0]
            imgname = os.path.join(wiki_path, imgname.lstrip('/'))
        png = imgname.replace(".svg", ".png")
        png = os.path.split(png)[1]
        png = "tex_out/%s" % png
        print(f"converting {imgname} to {png}", file=sys.stderr)
        subprocess.run(["inkscape", "-z", "-C", imgname,
                        "--export-png=%s" % png],
                       stdout=subprocess.PIPE)
        v[2][0] = png
        return Image(*v)
    if k == 'Str' and f == 'latex':
        # link page
        if not v.startswith("[["):
            return
        find_brack = v.find(']]')
        if find_brack == -1:
            return
        hashtag = ""
        link = v[2:find_brack]
        rest = link.split("#") # can be joined up again with '#'.join(rest)
        link = rest[0]
        out.write("     link %s\n" % link)
        lookups = {'sv': 'Scalable Vectors for Power ISA',
                   'SV|sv': 'Scalable Vectors for Power ISA',
                   'openpower/sv': 'Scalable Vectors for Power ISA',
                   'sv/overview': 'Overview Chapter',
                   'sv/vector_isa_comparison': 'Vector ISA Comparison',
                   'sv/comparison_table': 'ISA Comparison Table',
                   'sv/compliancy_levels': 'Compliancy Levels',
                   'sv/svp64': 'SVP64 Chapter',
                   'svp64': 'SVP64 Chapter',
                   'sv/sprs': 'SPRs',
                   'sv/normal': 'Arithmetic Mode',
                   'sv/ldst': 'Load/Store Mode',
                   'sv/cr_ops': 'Condition Register Fields Mode',
                   'sv/executive_summary': 'Exevutive Summary',
                   'sv/branches': 'Branch Mode',
                   'sv/setvl': 'setvl instruction',
                   'sv/svstep': 'svstep instruction',
                   'sv/remap': 'REMAP subsystem',
                   'sv/remap/appendix': 'REMAP Appendix',
                   'sv/mv.swizzle': 'Swizzle Move',
                   'sv/twin_butterfly': 'Twin Butterfly',
                   'sv/mv.vec': 'Pack / Unpack',
                   'svp64/appendix': 'SVP64 Appendix',
                   'sv/svp64/appendix': 'SVP64 Appendix',
                   'sv/svp64_quirks': 'SVP64 Quirks',
                   'openpower/isa/simplev': 'Simple-V pseudocode',
                   'opcode_regs_deduped': 'SVP64 Augmentation Table',
                   'sv/av_opcodes': 'Audio and Video Opcodes',
                   'av_opcodes': 'Audio and Video Opcodes',
                   'sv/vector_ops': 'SV Vector ops',
                   'sv/int_fp_mv': 'FP/Int Conversion ops',
                   'sv/bitmanip': 'Bitmanip ops',
                   'sv/cr_int_predication': 'CR Weird ops',
                   'cr_int_predication': 'CR Weird ops',
                   'sv/fclass': 'FP Class ops',
                   'sv/biginteger': 'Big Integer',
                   'sv/biginteger/analysis': 'Big Integer Analysis',
                   'isa/svfparith': 'Floating Point pseudocode',
                   'isa/svfixedarith': 'Fixed Point pseudocode',
                   'openpower/isa/branch': 'Branch pseudocode',
                   'openpower/transcendentals': 'Transcendentals',
                   }
        if link.startswith("ls") and link[2].isdigit():
            out.write("     found RFC %s\n" % link)
            return [Link(['', [], []],
                         [Str("{RFC "+link+"}")],
                         ['#%s' % link, '']), 
                    Str(v[find_brack+2:])]
        if link in lookups:
            out.write("     found %s\n" % lookups[link])
            return [Link(['', [], []],
                         [Str("{"+lookups[link]+"}")],
                         ['#%s' % linklookups(rest), '']),
                    Str(v[find_brack+2:])]
        if '|' in link:
            link, ref = link.split("|")
            return [Link(['', [], []],
                         [Str(link)],
                         [ref, '']), 
                    Str(v[find_brack+2:])]
    if k == 'Link':
        out.write("     link type %s\n" %
                  (type(v[1][0])))
    if k == 'RawInline' and v[0] == 'html':
        if re.fullmatch(r"< *br */? *>", v[1]):
            return [RawInline('latex', r'\\')]
        if re.fullmatch(r"< *sup *>", v[1]):
            return [RawInline('latex', r'\textsuperscript{')]
        if re.fullmatch(r"< */ *sup *>", v[1]):
            return [RawInline('latex', '}')]
        if re.fullmatch(r"< *sub *>", v[1]):
            return [RawInline('latex', r'\textsubscript{')]
        if re.fullmatch(r"< */ *sub *>", v[1]):
            return [RawInline('latex', '}')]
        if re.fullmatch(r"< *small *>", v[1]):
            return [RawInline('latex', r'{\small ')]
        if re.fullmatch(r"< */ *small *>", v[1]):
            return [RawInline('latex', '}')]

def linklookups(rest):
    res = '#'.join(rest)
    if len(rest[0]) == 0:
        res = '_'.join(rest[1:])
    return res

if __name__ == "__main__":
    toJSONFilter(inlinenotes)
