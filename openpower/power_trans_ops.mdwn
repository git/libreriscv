# Opcode Tables for PO=59/63 XO=1---011--

Power ISA v3.1B opcodes extracted from:

* Power ISA v3.1B Appendix D Table 23 sheet 2/3 of 4 page 1391/1392
* Power ISA v3.1B Appendix D Table 25 sheet 2/3 of 4 page 1399/1400

Parenthesized entries are not part of fptrans.

* Entries whose mnemonic ends in `s` are only in PO=59.
* Entries whose mnemonic does not end in `s` are only in PO=63.
* Entries whose mnemonic ends in `(s)` are in both PO=59 and PO=63.

| XO LSB half &#x2192;<br> XO MSB half &#x2193; | 01100                                                | 01101                                             | 01110                                                | 01111                                              |
|-----------------------------------------------|------------------------------------------------------|---------------------------------------------------|------------------------------------------------------|----------------------------------------------------|
| 10000                                         | <small>`10000 01100`</small><br>fcbrt(s) (draft)     | <small>`10000 01101`</small><br>fsinpi(s) (draft) | <small>`10000 01110`</small><br>fatan2pi(s) (draft)  | <small>`10000 01111`</small><br>fasinpi(s) (draft) |
| 10001                                         | <small>`10001 01100`</small><br>fcospi(s) (draft)    | <small>`10001 01101`</small><br>ftanpi(s) (draft) | <small>`10001 01110`</small><br>facospi(s) (draft)   | <small>`10001 01111`</small><br>fatanpi(s) (draft) |
| 10010                                         | <small>`10010 01100`</small><br>frsqrt(s) (draft)    | <small>`10010 01101`</small><br>fsin(s) (draft)   | <small>`10010 01110`</small><br>fatan2(s) (draft)    | <small>`10010 01111`</small><br>fasin(s) (draft)   |
| 10011                                         | <small>`10011 01100`</small><br>fcos(s) (draft)      | <small>`10011 01101`</small><br>ftan(s) (draft)   | <small>`10011 01110`</small><br>facos(s) (draft)     | <small>`10011 01111`</small><br>fatan(s) (draft)   |
| 10100                                         | <small>`10100 01100`</small><br>frecip(s) (draft)    | <small>`10100 01101`</small><br>fsinh(s) (draft)  | <small>`10100 01110`</small><br>fhypot(s) (draft)    | <small>`10100 01111`</small><br>fasinh(s) (draft)  |
| 10101                                         | <small>`10101 01100`</small><br>fcosh(s) (draft)     | <small>`10101 01101`</small><br>ftanh(s) (draft)  | <small>`10101 01110`</small><br>facosh(s) (draft)    | <small>`10101 01111`</small><br>fatanh(s) (draft)  |
| 10110                                         | <small>`10110 01100`</small><br>&nbsp;               | <small>`10110 01101`</small><br>&nbsp;            | <small>`10110 01110`</small><br>&nbsp;               | <small>`10110 01111`</small><br>&nbsp;             |
| 10111                                         | <small>`10111 01100`</small><br>&nbsp;               | <small>`10111 01101`</small><br>&nbsp;            | <small>`10111 01110`</small><br>&nbsp;               | <small>`10111 01111`</small><br>&nbsp;             |

| XO LSB half &#x2192;<br> XO MSB half &#x2193; | 01100                                              | 01101                                              | 01110                                                   | 01111                                                   |
|-----------------------------------------------|----------------------------------------------------|----------------------------------------------------|---------------------------------------------------------|---------------------------------------------------------|
| 11000                                         | <small>`11000 01100`</small><br>fexp2m1(s) (draft) | <small>`11000 01101`</small><br>flog2p1(s) (draft) | <small>`11000 01110`</small><br>(cffpro) (draft)       | <small>`11000 01111`</small><br>(ctfpr(s)) (draft)     |
| 11001                                         | <small>`11001 01100`</small><br>fexpm1(s) (draft)  | <small>`11001 01101`</small><br>flogp1(s) (draft)  | <small>`11001 01110`</small><br>(fctid)                 | <small>`11001 01111`</small><br>(fctidz)                |
| 11010                                         | <small>`11010 01100`</small><br>fexp10m1(s) (draft)| <small>`11010 01101`</small><br>flog10p1(s) (draft)| <small>`11010 01110`</small><br>(fcfid(s))              | <small>`11010 01111`</small><br>fmod(s) (draft)         |
| 11011                                         | <small>`11011 01100`</small><br>fpown(s) (draft)   | <small>`11011 01101`</small><br>frootn(s) (draft)  | <small>`11011 01110`</small><br>&nbsp;                  | <small>`11011 01111`</small><br>&nbsp;                  |
| 11100                                         | <small>`11100 01100`</small><br>fexp2(s) (draft)   | <small>`11100 01101`</small><br>flog2(s) (draft)   | <small>`11100 01110`</small><br>(mffpr(s)) (draft)      | <small>`11100 01111`</small><br>(mtfpr(s)) (draft)      |
| 11101                                         | <small>`11101 01100`</small><br>fexp(s) (draft)    | <small>`11101 01101`</small><br>flog(s) (draft)    | <small>`11101 01110`</small><br>(fctidu)                | <small>`11101 01111`</small><br>(fctiduz)               |
| 11110                                         | <small>`11110 01100`</small><br>fexp10(s) (draft)  | <small>`11110 01101`</small><br>flog10(s) (draft)  | <small>`11110 01110`</small><br>(fcfidu(s))             | <small>`11110 01111`</small><br>fremainder(s) (draft)   |
| 11111                                         | <small>`11111 01100`</small><br>fpowr(s) (draft)   | <small>`11111 01101`</small><br>fpow(s) (draft)    | <small>`11111 01110`</small><br>&nbsp;                  | <small>`11111 01111`</small><br>&nbsp;                  |

| XO LSB half &#x2192;<br> XO MSB half &#x2193; | 10000                                           | 10001                                  | 10010                                     | 10011                                  |
|-----------------------------------------------|-------------------------------------------------|----------------------------------------|-------------------------------------------|----------------------------------------|
| ////0                                         | <small>`....0 10000`</small><br>fminmax (draft) | <small>`////0 10001`</small><br>&nbsp; | <small>`////0 10010`</small><br>(fdiv(s)) | <small>`////0 10011`</small><br>&nbsp; |
| ////1                                         | <small>`////1 10000`</small><br>&nbsp;          | <small>`////1 10001`</small><br>&nbsp; | <small>`////1 10010`</small><br>(fdiv(s)) | <small>`////1 10011`</small><br>&nbsp; |

# DRAFT List of 2-arg opcodes

These are X-Form, recommended Major Opcode 63 for full-width
and 59 for half-width (ending in s).

| 0.5|6.10|11.15|16.20| 21..30      |31| name           |  Form   |
| -- | -- | --- | --- | ----------  |--| ----           | ------- |
| NN |FRT | FRA | FRB | 1xxxx011xx  |Rc| transcendental | X-Form  |
| NN |FRT | FRA | RB  | 1xxxx011xx  |Rc| transcendental | X-Form  |
| NN |FRT | FRA | FRB | xxxxx10000  |Rc| transcendental | X-Form  |

Recommended 10-bit XO assignments:

| opcode          | Description                             | Major 59 and 63 | bits 16..20 |
|-----------------|-----------------------------------------|-----------------|-------------|
| fatan2(s)       | atan2 arc tangent                       | 10010 01110     | FRB         |
| fatan2pi(s)     | atan2 arc tangent / &pi;                | 10000 01110     | FRB         |
| fpow(s)         | x<sup>y</sup>                           | 11111 01101     | FRB         |
| fpown(s)        | x<sup>n</sup> (n &in; &#x2124;)         | 11011 01100     | RB          |
| fpowr(s)        | x<sup>y</sup> (x >= 0)                  | 11111 01100     | FRB         |
| frootn(s)       | <sup>n</sup>&#x221A;x (n &in; &#x2124;) | 11011 01101     | RB          |
| fhypot(s)       | &#x221A;(x<sup>2</sup> + y<sup>2</sup>) | 10100 01110     | FRB         |
| fminmax         | min/max                                 | ....0 10000     | FRB         |
| fmod(s)         | modulus                                 | 11010 01111     | FRB         |
| fremainder(s)   | IEEE 754 remainder                      | 11110 01111     | FRB         |

# DRAFT List of 1-arg transcendental opcodes

These are X-Form, and are mostly identical in Special Registers Altered to
`fsqrt` (the exact fp exceptions they can produce differ).
Recommended Major Opcode 63 for full-width and 59 for half-width (ending in s).

Special Registers Altered (FIXME: come up with correct list):

    FPRF FR FI FX OX UX XX
    VXSNAN VXIMZ VXZDZ
    CR1                    (if Rc=1)

| 0.5|6.10|11.15|16.20| 21..30      |31| name      |  Form   |
| -- | -- | --- | --- | ----------  |--| ----      | ------- |
| NN |FRT | /// | FRB  | 1xxxx011xx |Rc| transcendental | X-Form  |

Recommended 10-bit XO assignments:

| opcode     | Description              | Major 59 and 63 |
|------------|--------------------------|-----------------|
| frsqrt(s)  | 1 / &#x221A;x            | 10010 01100     |
| fcbrt(s)   | &#x221B;x                | 10000 01100     |
| frecip(s)  | 1 / x                    | 10100 01100     |
| fexp2m1(s) | 2<sup>x</sup> - 1        | 11000 01100     |
| flog2p1(s) | log<sub>2</sub> (x + 1)  | 11000 01101     |
| fexp2(s)   | 2<sup>x</sup>            | 11100 01100     |
| flog2(s)   | log<sub>2</sub> x        | 11100 01101     |
| fexpm1(s)  | e<sup>x</sup> - 1        | 11001 01100     |
| flogp1(s)  | log<sub>e</sub> (x + 1)  | 11001 01101     |
| fexp(s)    | e<sup>x</sup>            | 11101 01100     |
| flog(s)    | log<sub>e</sub> x        | 11101 01101     |
| fexp10m1(s)| 10<sup>x</sup> - 1       | 11010 01100     |
| flog10p1(s)| log<sub>10</sub> (x + 1) | 11010 01101     |
| fexp10(s)  | 10<sup>x</sup>           | 11110 01100     |
| flog10(s)  | log<sub>10</sub> x       | 11110 01101     |

# DRAFT List of 1-arg trigonometric opcodes

These are X-Form, and are mostly identical in Special Registers Altered to
`fsqrt` (the exact fp exceptions they can produce differ).
Recommended Major Opcode 63 for full-width and 59 for half-width (ending in s)

Special Registers Altered:

    FPRF FR FI FX OX UX XX
    VXSNAN VXIMZ VXZDZ
    CR1                    (if Rc=1)

| 0.5|6.10|11.15|16.20| 21..30      |31| name      |  Form   |
| -- | -- | --- | --- | ----------  |--| ----      | ------- |
| NN |FRT | /// | FRB  | 1xxxx011xx |Rc| trigonometric | X-Form  |

Recommended 10-bit XO assignments:

| opcode      | Description              | Major 59 and 63  |
|-------------|--------------------------|------------------|
| fsin(s)     | sin (radians)            | 10010 01101      |
| fcos(s)     | cos (radians)            | 10011 01100      |
| ftan(s)     | tan (radians)            | 10011 01101      |
| fasin(s)    | arcsin (radians)         | 10010 01111      |
| facos(s)    | arccos (radians)         | 10011 01110      |
| fatan(s)    | arctan (radians)         | 10011 01111      |
| fsinpi(s)   | sin(&pi; * x)            | 10000 01101      |
| fcospi(s)   | cos(&pi; * x)            | 10001 01100      |
| ftanpi(s)   | tan(&pi; * x)            | 10001 01101      |
| fasinpi(s)  | arcsin(x) / &pi;         | 10000 01111      |
| facospi(s)  | arccos(x) / &pi;         | 10001 01110      |
| fatanpi(s)  | arctan(x) / &pi;         | 10001 01111      |
| fsinh(s)    | hyperbolic sin           | 10100 01101      |
| fcosh(s)    | hyperbolic cos           | 10101 01100      |
| ftanh(s)    | hyperbolic tan           | 10101 01101      |
| fasinh(s)   | inverse hyperbolic sin   | 10100 01111      |
| facosh(s)   | inverse hyperbolic cos   | 10101 01110      |
| fatanh(s)   | inverse hyperbolic tan   | 10101 01111      |
