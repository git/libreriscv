# SVP64 "Cookbook"

To add a new cookbook recipe, ensure to add a '''svp64_cookbook''' tag so
that the recipe appears automatically on this page. 

[[!inline pages="tagged(svp64_cookbook)" actions="no" archive="yes" quick="yes"
          postform=yes postformtext="Type the title of the recipe:"
]]
