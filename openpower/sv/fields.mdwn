# Power ISA Fields

These were originally taken from Power ISA v3.0B PDF, retain the Section
Numbering from the original Power ISA v3.0B Specification PDF,
and are in machine-readable
format that may be parsed with the following program:
[power_fields.py](https://git.libre-soc.org/?p=openpower-isa.git;a=blob;f=src/openpower/decoder/power_fields.py;hb=HEAD)

Some additions have been made for DRAFT Scalar instructions Forms:
BM2-Form, TLI-Form and others. Other additions are for SVP64 such
as SVM-Form, SVL-Form.

[[!inline pages="openpower/isatables/fields.text" raw="yes" ]]
