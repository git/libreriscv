# OPF External RFCs

Funded by NLnet Grants as part of EU Horizon 2023: NGI Entrust No 101069594

These are Draft Proposals only that are in the process of being
written and submitted to the
[OpenPOWER Foundation ISA TWG](https://openpower.foundation/groups/isa/).
They are not part of the official Power ISA unless ratified by the
OPF ISA TWG.
[Power ISA is Trademarked](https://trademarks.justia.com/789/79/power-78979814.html)
and use of the ISA covered by an 
[Open EULA](https://openpowerfoundation.org/blog/final-draft-of-the-power-isa-eula-released/)
Draft unofficial proposals in PDF form may be found on the
[Libre-SOC FTP site](https://ftp.libre-soc.org/opf_ext_rfc)

Constructive Feedback on Libre-SOC Draft Proposals is best discussed on the
[Libre-SOC ISA mailing list](http://lists.libre-soc.org/mailman/listinfo/libre-soc-isa). Note that once RFCs are formally submitted, involvement in
their discussion within the OPF ISA TWG first requires OPF
Membership, and once joined a request to be submitted to join the ISA TWG.
Actual submission of an External RFC however does **not** require OPF Membership.

To add a new RFC, take a copy of an existing RFC and use it as a template,
ensure to keep the '''opf_rfc''' tagging so that the RFC appears
automatically on this page.  In the box below type the next available
number "lsNNN" to begin creating a new page.

Please note: research, writing, development, and any discussion of RFCs before 25oct2022
(ls001, ls002, ls003) do not qualify for NLnet funding under 
[[nlnet_2022_opf_isa_wg]] because NLnet Grants are not retrospective.
However *further* work on those 3 RFCs *does* qualify (because the
further work is initiated after 25oct2022, the Grant Approval date).

[[!inline pages="tagged(opf_rfc)" actions="no" archive="yes" quick="yes"
          postform=yes postformtext="Copy the template, enter new RFC number:"
]]
