def sif(RA, mask=None, zero=False):
    RT = RA if mask is not None and not zero else 0
    i = 0
    # start setting if no predicate or if 1st predicate bit set
    setting_mode = mask is None
    while i < 16:
        bit = 1<<i
        if not setting_mode and mask is not None and (mask & bit):
            setting_mode = True # back into "setting" mode
        if setting_mode and mask is not None and not (mask & bit):
            setting_mode = False # masked out, stop setting
        if setting_mode:
            RT |= bit    # inclusively set bit in RT
            if RA & bit: # found a bit in rs1: stop setting RT
                setting_mode = False
        i += 1
    return RT

if __name__ == '__main__':
     m  = 0b11000011
     v3 = 0b10010100 # vmsif.m v2, v3
     v2 = 0b11000011 # v2
     RT = sif(v3, m, zero=True)
     print(bin(v3), bin(v2), bin(RT))
     v3 = 0b10010100 # vmsif.m v2, v3
     v2 = 0b00000111 # v2 contents
     RT = sif(v3)
     print(bin(v3), bin(v2), bin(RT))
     v3 = 0b10010101 # vmsif.m v2, v3
     v2 = 0b00000001 # v2
     RT = sif(v3)
     print(bin(v3), bin(v2), bin(RT))
     v3 = 0b00000000 # vmsif.m v2, v3
     v2 = 0b11111111 # v2
     RT = sif(v3)
     print(bin(v3), bin(v2), bin(RT))
